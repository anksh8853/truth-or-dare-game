package com.tabletd.truthdare;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private AdView mAdView2;;
    private ImageView img_bottle, img_circle;
    private int lastPosition, newPosition, maxRotationPos, minRotationPos, rotationDurationPos, duration, minRotation, maxRotation;
    private Random random=new Random();


    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private int[] imgCircleIds={R.drawable.circle2, R.drawable.circle3, R.drawable.circle4, R.drawable.circle5, R.drawable.circle6};
    private final Integer[] minRotationsList ={1,3,5,7,9,11,13};
    private final Integer[] maxRotationsList ={2,4,6,8,10,12,14,16};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView2 = findViewById(R.id.adView2);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView2.loadAd(adRequest);



        img_bottle=findViewById(R.id.img_bottle);
        img_circle=findViewById(R.id.img_circle);



        sharedPreferences=getSharedPreferences("settings", MODE_PRIVATE);
        editor= sharedPreferences.edit();

        img_circle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                newPosition=random.nextInt(maxRotation-minRotation)+minRotation;
                int pivoitX=img_bottle.getWidth()/2;
                int pivoitY=img_bottle.getHeight()/2;
                Animation rotate= new RotateAnimation(lastPosition, newPosition, pivoitX, pivoitY);
                rotate.setDuration(duration);
                rotate.setFillAfter(true);
                img_bottle.startAnimation(rotate);
            }
        });


    }




    public void openSettings(View view) {
        Intent intent=new Intent(this, User_Setting.class);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        int playersCountPosition=sharedPreferences.getInt("playersCountPosition", 0);
        img_circle.setImageResource(imgCircleIds[playersCountPosition]);
        minRotationPos=sharedPreferences.getInt("minRotationPos", 1);
        maxRotationPos=sharedPreferences.getInt("maxRotationPos", 4);
        rotationDurationPos=sharedPreferences.getInt("rotationDurationPos", 1);
        duration=((rotationDurationPos+1)*2)*1000;
        minRotation=minRotationsList[minRotationPos]*360;
        maxRotation=maxRotationsList[maxRotationPos]*360;

    }
}
